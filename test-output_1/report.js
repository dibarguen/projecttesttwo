$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("src/test/resources/com/grupoaval/portalnuevastecnologias/features/AgregarProductoAlCarrito.feature");
formatter.feature({
  "name": "Agregar producto al carrito de compras",
  "description": "  Como usuario dentro del sistema, quiero adicionar productos al carrito de compras",
  "keyword": "Característica"
});
formatter.background({
  "name": "",
  "description": "",
  "keyword": "Antecedentes"
});
formatter.step({
  "name": "que Usuario ingresa al portal nuevas tecnologias",
  "keyword": "Dado "
});
formatter.match({
  "location": "InicioDeSesionStepDefinitions.queDavidIntreAlPortalAFC()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "ingresa su usuario y clave",
  "rows": [
    {
      "cells": [
        "Usuario",
        "Clave"
      ]
    },
    {
      "cells": [
        "oswaldo",
        "1233903960"
      ]
    }
  ],
  "keyword": "Cuando "
});
formatter.match({
  "location": "InicioDeSesionStepDefinitions.ingresaSuUsuarioYClave(Usuario\u003e)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "presiona la tecla Enter",
  "keyword": "Y "
});
formatter.match({
  "location": "InicioDeSesionStepDefinitions.presionaLaTeclaEnter()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Validar que agregue,sume y quite cantidad de productos",
  "description": "",
  "keyword": "Escenario",
  "tags": [
    {
      "name": "@SmokeTest"
    }
  ]
});
formatter.step({
  "name": "selecciona una categoria",
  "rows": [
    {
      "cells": [
        "audio"
      ]
    }
  ],
  "keyword": "Cuando "
});
formatter.match({
  "location": "AgregarProductoStepDefinitions.seleccioneCategoria(Carrito\u003e)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "selecciona el producto",
  "rows": [
    {
      "cells": [
        "Celular XIAOMI Mi A2 Lite DS 4G Dorado"
      ]
    }
  ],
  "keyword": "Y "
});
formatter.match({
  "location": "AgregarProductoStepDefinitions.seleccionaProducto(String\u003e)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "se agrega al carrito",
  "keyword": "Y "
});
formatter.match({
  "location": "AgregarProductoStepDefinitions.agregaProducto()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "valida que aparezca el siguiente mensaje:",
  "rows": [
    {
      "cells": [
        "El producto Celular XIAOMI Mi A2 Lite DS 4G Dorado ha sido añadido al carrito."
      ]
    }
  ],
  "keyword": "Entonces "
});
formatter.match({
  "location": "AgregarProductoStepDefinitions.validarPopupProductoAgregado(String\u003e)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "escoge otra categoria",
  "rows": [
    {
      "cells": [
        "celulares"
      ]
    }
  ],
  "keyword": "Y "
});
formatter.match({
  "location": "AgregarProductoStepDefinitions.seleccionarOtraCategoria(Carrito\u003e)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "selecciona el producto",
  "rows": [
    {
      "cells": [
        "Audífonos Sony tipo banda para la cabeza - MDR-ZX110"
      ]
    }
  ],
  "keyword": "Cuando "
});
formatter.match({
  "location": "AgregarProductoStepDefinitions.seleccionaProducto(String\u003e)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "se agrega al carrito",
  "keyword": "Y "
});
formatter.match({
  "location": "AgregarProductoStepDefinitions.agregaProducto()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "valida que aparezca el siguiente mensaje:",
  "rows": [
    {
      "cells": [
        "El producto Audífonos inalámbricos EXTRA BASS™ XB650BT ha sido añadido al carrito."
      ]
    }
  ],
  "keyword": "Entonces "
});
formatter.match({
  "location": "AgregarProductoStepDefinitions.validarPopupProductoAgregado(String\u003e)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "valida que aparezca el producto agregado en el apartado de items del carrito",
  "keyword": "Y "
});
formatter.match({
  "location": "AgregarProductoStepDefinitions.vadidarProductoItem()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "se ingresa al menu de Cuenta",
  "keyword": "Cuando "
});
formatter.match({
  "location": "AgregarProductoStepDefinitions.cickMenuCuenta()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "selecciona ver carrito",
  "keyword": "Y "
});
formatter.match({
  "location": "AgregarProductoStepDefinitions.clickVerCarrito()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "valida suma total productos",
  "keyword": "Y "
});
formatter.match({
  "location": "AgregarProductoStepDefinitions.validarTotalProductosCarrito()"
});
formatter.result({
  "error_message": "java.lang.AssertionError: El valor capturado no corresponde con del descuento aplicado\r\n\tat com.grupoaval.portalnuevastecnologias.pageobjects.AgregarProductoAlCarritoPage.sumaTotalProductosCarrito(AgregarProductoAlCarritoPage.java:207)\r\n\tat com.grupoaval.portalnuevastecnologias.steps.AgregarProductoAlCarritoSteps.validarSumaCarrito(AgregarProductoAlCarritoSteps.java:168)\r\n\tat com.grupoaval.portalnuevastecnologias.steps.AgregarProductoAlCarritoSteps$$EnhancerByCGLIB$$3d72818c.CGLIB$validarSumaCarrito$6(\u003cgenerated\u003e)\r\n\tat com.grupoaval.portalnuevastecnologias.steps.AgregarProductoAlCarritoSteps$$EnhancerByCGLIB$$3d72818c$$FastClassByCGLIB$$3a2f0a5e.invoke(\u003cgenerated\u003e)\r\n\tat net.sf.cglib.proxy.MethodProxy.invokeSuper(MethodProxy.java:228)\r\n\tat net.thucydides.core.steps.StepInterceptor.invokeMethod(StepInterceptor.java:460)\r\n\tat net.thucydides.core.steps.StepInterceptor.executeTestStepMethod(StepInterceptor.java:445)\r\n\tat net.thucydides.core.steps.StepInterceptor.runTestStep(StepInterceptor.java:420)\r\n\tat net.thucydides.core.steps.StepInterceptor.runOrSkipMethod(StepInterceptor.java:175)\r\n\tat net.thucydides.core.steps.StepInterceptor.testStepResult(StepInterceptor.java:162)\r\n\tat net.thucydides.core.steps.StepInterceptor.intercept(StepInterceptor.java:68)\r\n\tat com.grupoaval.portalnuevastecnologias.steps.AgregarProductoAlCarritoSteps$$EnhancerByCGLIB$$3d72818c.validarSumaCarrito(\u003cgenerated\u003e)\r\n\tat com.grupoaval.portalnuevastecnologias.stepdefinitions.AgregarProductoStepDefinitions.validarTotalProductosCarrito(AgregarProductoStepDefinitions.java:184)\r\n\tat ✽.valida suma total productos(src/test/resources/com/grupoaval/portalnuevastecnologias/features/AgregarProductoAlCarrito.feature:33)\r\n",
  "status": "failed"
});
formatter.step({
  "name": "quita cantidad de productos",
  "keyword": "Entonces "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.background({
  "name": "",
  "description": "",
  "keyword": "Antecedentes"
});
formatter.step({
  "name": "que Usuario ingresa al portal nuevas tecnologias",
  "keyword": "Dado "
});
formatter.match({
  "location": "InicioDeSesionStepDefinitions.queDavidIntreAlPortalAFC()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "ingresa su usuario y clave",
  "rows": [
    {
      "cells": [
        "Usuario",
        "Clave"
      ]
    },
    {
      "cells": [
        "oswaldo",
        "1233903960"
      ]
    }
  ],
  "keyword": "Cuando "
});
formatter.match({
  "location": "InicioDeSesionStepDefinitions.ingresaSuUsuarioYClave(Usuario\u003e)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "presiona la tecla Enter",
  "keyword": "Y "
});
formatter.match({
  "location": "InicioDeSesionStepDefinitions.presionaLaTeclaEnter()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Validar total producto en desplegable",
  "description": "",
  "keyword": "Escenario",
  "tags": [
    {
      "name": "@SmokeTest"
    }
  ]
});
formatter.step({
  "name": "selecciona una categoria",
  "rows": [
    {
      "cells": [
        "audio"
      ]
    }
  ],
  "keyword": "Cuando "
});
formatter.match({
  "location": "AgregarProductoStepDefinitions.seleccioneCategoria(Carrito\u003e)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "selecciona el producto",
  "rows": [
    {
      "cells": [
        "Celular XIAOMI Mi A2 Lite DS 4G Dorado"
      ]
    }
  ],
  "keyword": "Y "
});
formatter.match({
  "location": "AgregarProductoStepDefinitions.seleccionaProducto(String\u003e)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "se agrega al carrito",
  "keyword": "Y "
});
formatter.match({
  "location": "AgregarProductoStepDefinitions.agregaProducto()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Valida el valor total de los productos en desplegable carrito",
  "keyword": "Entonces "
});
formatter.match({
  "location": "AgregarProductoStepDefinitions.validarTotalDesplegable()"
});
formatter.result({
  "status": "passed"
});
formatter.background({
  "name": "",
  "description": "",
  "keyword": "Antecedentes"
});
formatter.step({
  "name": "que Usuario ingresa al portal nuevas tecnologias",
  "keyword": "Dado "
});
